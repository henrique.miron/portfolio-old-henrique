<?php
	include 'mailer.php';

	$mail->setFrom($email, $name);
	// $mail->addAddress('suporte@ad4pixels.com.br');
    $mail->Subject = 'Ticket Site - '.$subject;

    $message   = nl2br($message);
    $date_time = date('d/m/y às H:i');

	$count = count($_FILES['attachments']['tmp_name']);
	$files = [];

	for ($i=0; $i < $count; $i++)
		foreach ($_FILES['attachments'] as $key => $value)
			$files[$i][$key] = $value[$i];

	foreach ($files as $file)
		if ($file['error'] == UPLOAD_ERR_OK)
			$mail->AddAttachment($file['tmp_name'], $file['name']);

	$mail->Body    = <<<HTML
        {$message}
        <hr>
        Ticket criado através da página de suporte<br>
        Em {$date_time} por {$name}, da empresa {$company}.
HTML;
	if(!$mail->send()) {
		header('Location: ../../falha');
	} else {
		header('Location: ../../obrigado');
	}
