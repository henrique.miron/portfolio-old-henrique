<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title></title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700" rel="stylesheet">
		<link rel="stylesheet" href="css/bootstrap.css"> 
		<link rel="stylesheet" href="css/style.css"> 
	</head>
    <body>
	<header>
		<div class="container">
			<div class="row flex-align">
				<div class="col-md-6">
					<img src="img/logo-top.png" alt="" class="img-responsive">
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<form>
						<div class="form-group">
							<label for="empresaNome">Nome da Empresa</label>
							<input type="text" class="form-control" name="empresaNome" id="empresaNome" placeholder="Nome da Empresa">
						</div>
						<div class="form-group">
							<label for="contatoNome">Nome do Contato</label>
							<input type="text" class="form-control" name="contatoNome" id="contatoNome" placeholder="Nome do Contato">
						</div>
						<div class="form-group">
							<label for="contatoCargo">Cargo do Contato</label>
							<input type="text" class="form-control" name="contatoCargo" id="contatoCargo" placeholder="Cargo do Contato">
						</div>
						<div class="form-group">
							<label for="contatoTelefone">Telefone do Contato</label>
							<input type="text" class="form-control" name="contatoTelefone" id="contatoTelefone" placeholder="Telefone do Contato">
						</div>
						<div class="form-group">
							<label for="contatoEmail">E-mail do Contato</label>
							<input type="text" class="form-control" name="contatoEmail" id="contatoEmail" placeholder="Email do Contato">
						</div>

						<div class="form-group">
							<label for="responsavelNome">Nome do Responsável</label>
							<input type="text" class="form-control" name="responsavelNome" id="responsavelNome" placeholder="Nome do Responsável">
						</div>
						<div class="form-group">
							<label for="responsavelCargo">Cargo do Responsável</label>
							<input type="text" class="form-control" name="responsavelCargo" id="responsavelCargo" placeholder="Cargo do Responsável">
						</div>
						<div class="form-group">
							<label for="responsavelTelefone">Telefone do Responsável</label>
							<input type="text" class="form-control" name="responsavelTelefone" id="responsavelTelefone" placeholder="Telefone do Responsável">
						</div>
						<div class="form-group">
							<label for="responsavelEmail">E-mail do Responsável</label>
							<input type="text" class="form-control" name="responsavelEmail" id="responsavelEmail" placeholder="Email do Responsável">
						</div>
						<div class="form-group">
							<label for="descricaoProjeto">Descrição do Projeto</label>
							<textarea class="form-control" rows="5" name="descricaoProjeto" id="descricaoProjeto" placeholder="Descrição do Projeto"></textarea>
						</div>
						<div class="form-group">
							<label for="custosProjeto">Custos do Projeto</label>
							<textarea class="form-control" rows="5" name="custosProjeto" id="custosProjeto" placeholder="Custos do Projeto"></textarea>
						</div>
						<button type="submit" class="btn btn-default">Enviar</button>
					</form>
				</div>
			</div>
		</div>
	</section>
		
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<p>4Pixels Agência Digital<br>
					R. Padre Estevão Pernet, 160 - CJ 409<br>
					4p@ad4pixels.com.br</p>
				</div>
			</div>
		</div>
	</footer>
        
    </body>
</html>