<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title></title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700" rel="stylesheet">
		<link rel="stylesheet" href="css/bootstrap.css"> 
		<link rel="stylesheet" href="css/style.css"> 
		<link rel="stylesheet" type="text/css" href="css/print.css" media="print">
	</head>
    <body>
	<header>		
		<div class="container">
			<div class="row flex-align">
				<div class="col-md-6">
					<img src="img/logo-top.png" alt="" class="img-responsive">
				</div>
				<div class="col-md-6 text-right">
					<span>Proposta para</span>
					<h1 id="titulo-empresa">Cardenas</h1>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-8 text-justify">
					<h2>Descrição do projeto</h2>
					<p>O objetivo é fornecer ao cliente uma ferramenta que permita total autonomia na criação das imagens, não sendo necessário usar nenhum outro editor de imagens. Também deve ser intuitiva, rápida e leve.</p>
					<p>Dentro desse sistema, a Cardenas irá disponibilizar periodicamente novos recursos, como imagens, fontes e símbolos. Alguns serão disponibilizados de forma gratuita, outros serão pagos, a maioria por volta de 2 reais. Os pagamentos serão realizados através do PagSeguro, usando a mesma conta da loja virtual.</p>
					<p>Nossa primeira missão é entregar um sistema onde seja possível criar algo sem a ajuda de nenhum outro software. Essa primeira etapa durará até dois meses, pois há pressa na utilização desse novo sistema melhorado.</p>
					<p>Uma ferramenta onde existem opções pré­-configuradas e uma maneira de alterar cores e fontes é o bastante por enquanto. Não é preciso alterar posições. O ideal é o sistema disponibilizar alguns "templates", onde o usuário escolha e as opções seriam aplicadas no modelo em construção.</p>
					<p>Algumas opções extras como deixar trabalhos salvos e retornar depois, abrir e finalizar é um diferencial. Faremos uma análise no banco de dados atual para definir se é precisso uma remodelagem completa, modernizando e melhorando a estrutura para o novo sistema. Todos os dados serão migrados.</p>
					<h2>Custos</h2>
					<p>Os valores pagos à 4Pixels são referentes aos serviços descritos acima. Nosso modelo de cobrança é calculado através das horas de trabalho estimadas para o projeto. Para este projeto foram estimadas 221 horas.</p>
					<p>O custo/hora do projeto será de R$ 64,93. É possível acompanhar em tempo real se o projeto está dentro do prazo e orçamento. Além disso, como conversado em reunião, apresentamos semanalmente os avanços do projeto, sempre com algum incremento utilizável.</p>
					<p>Total estimado de R$ 14.349,53, com prazo entre 6 e 7 semanas, a partir da primeira terça feira após as condições aprovadas.</p>
					<p>Todas as condições são firmadas por contrato e todas as operações financeiras acontecem com nota fiscal.</p>
				</div>
				<div class="col-md-4 text-right" id="editDocument">					
					<ul class="list-unstyled list-contato">
						<li><h2>Contato</h2></li>
						<li><h3>Daniel Mercadante</h3></li>	
						<li>Designer Gráfico</li>
						<li>comunicacao@cardenas.com.br</li>
						<li>+55 11 2601.2955</li>
						<li>+55 11 2601.9533</li>
					</ul>
					<ul class="list-unstyled list-contato">
						<li><h2>Responsável</h2></li>
						<li><h3>Roberto Gadducci</h3></li>	
						<li>Diretor Executivo</li>
						<li>roberto.gadducci@ad4pixels.com.br</li>
						<li>+55 11 99946.9645</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<p>Proposta válida por 30 dias corridos. Todos os dados deste documento são sigilosos e devem ser mantidos em segredo entre os “CONTATOS” e “RESPONSÁVEIS”.</p>
				</div>
				<div class="col-md-4 col-md-offset-2 text-left">
					<p>4Pixels Agência Digital<br>
					R. Padre Estevão Pernet, 160 - CJ 409<br>
					4p@ad4pixels.com.br</p>
				</div>
			</div>
		</div>
	</footer>
	 <script src="js/editable.js"></script> 		
    </body>
</html>