module.exports = function ( grunt )
{
	grunt.initConfig({
		sass: {
			dist: {
				files: {
					"../assets/css/base.css": "scss/base.scss",
				}
			}
		},
		copy: {
			snap: {
				src: "node_modules/snapsvg/dist/snap.svg-min.js",
				dest: "../assets/js/snap.svg-min.js",
			},
			jquery: {
				src: "node_modules/jquery/dist/jquery.min.js",
				dest: "../assets/js/jquery.min.js",
			},
			tether: {
				src: "node_modules/tether/dist/js/tether.min.js",
				dest: "../assets/js/tether.min.js",
			},
			bootstrap: {
				src: "node_modules/bootstrap/dist/js/bootstrap.min.js",
				dest: "../assets/js/bootstrap.min.js",
			},
			angulartics: {
				src: [
					"node_modules/angulartics/dist/angulartics.min.js",
					"node_modules/angulartics-google-analytics/dist/angulartics-ga.min.js",
				],
				dest: "../assets/js/",
				flatten: true,
				filter: 'isFile',
				expand: true,
			},
			fp: {
				src: "js/*",
				dest: "../assets/",
			},
		},
		cssmin: {
			options: {
				keepSpecialComments: 0
			},
			target: {
				files: [{
					expand: true,
					cwd: "../assets/css",
					src: ["*.css", "!*.min.css"],
					dest: "../assets/css",
					ext: ".min.css"
				}]
			}
		},
		uglify: {
			my_target: {
				files: {
					"../assets/js/4p.min.js": [
						"js/4p.js",
						"js/bdr.js"
					],
					"../assets/js/any.js": [
						"../assets/js/jquery.min.js",
						"node_modules/angular/angular.min.js",
						"node_modules/angular-resource/angular-resource.min.js",
						"node_modules/angular-route/angular-route.min.js",
						"../assets/js/snap.svg-min.js",
						"../assets/js/tether.min.js",
						"../assets/js/bootstrap.min.js"
					]
				}
			}
		},
		watch: {
			css: {
				files: [
					"scss/*.scss"
				],
				tasks: [
					"sass"
				],
			},
			js: {
				files: [
					"js/*.js"
				],
				tasks: [
					"copy",
					"uglify"
				]
			}
		},
	});

	grunt.loadNpmTasks( "grunt-contrib-sass" );
	grunt.loadNpmTasks( "grunt-contrib-copy" );
	grunt.loadNpmTasks( "grunt-contrib-cssmin" );
	grunt.loadNpmTasks( "grunt-contrib-uglify" );
	grunt.loadNpmTasks( "grunt-contrib-watch" );

	grunt.registerTask( "default", [ "sass", "copy", "cssmin", "uglify", "watch" ] );
};
