var makeBdr = function ()
{
	var menu = document.getElementById( "nav-global" );
	var bdr  = Snap( "#nav-global-bdr" );

	if ( !bdr )
		return;

	bdr.clear();

	var g = bdr.gradient("L(0, 0, 0, " + menu.clientHeight + ")#7c4d8e-#009966-#bf0410-#d68400");
	g.node.id = g.id;

	bdr.path( "M " + ( menu.clientWidth - 8 ) + ",0 L " + (menu.clientWidth) + ",0 L " + (menu.clientWidth) + "," + menu.clientHeight + " L " + ( menu.clientWidth - 8 ) + "," + menu.clientHeight );
	bdr.attr({
		fill: "url(" + window.location.href + "#" + g.id + ")",
	});
}

window.setInterval(
	function ()
	{
		makeBdr();
	},
	20
);
