(function ( angular ) {

angular
	.module(
		"4p",
		[
			"ngRoute",
			"ngResource",

			"angulartics",
			"angulartics.google.analytics",
		]
	)
	.constant(
		"menuItems",
		window.__menuItems
	)
	.config(
		function
		(
			$locationProvider,
			$routeProvider,
			menuItems
		)
		{
			$locationProvider.html5Mode( true );

			$routeProvider
				.otherwise(
					{
						controller: "MenuController",
						templateUrl: function ( params )
						{
							var l = window.location.href;
							var b = document.baseURI;

							var url = l.replace( b, "" );

							if ( "/" == url.charAt(url.length - 1) )
								url = url.replace( /\/$/, "" );

							for (var i = 0; i < menuItems.length; i++) {
								if ( menuItems[i].url == url && menuItems[i].default )
									url = menuItems[i].url + "/" + menuItems[i].default;
							}

							if ( "" == url )
								url = "index";

							return "views/" + url + ".html";
						},
						resolve: {
							_current: function ( $location )
							{
								var main = $location.path().substring( 1 );
								var slash = main.indexOf( "/" );

								var path = ( slash >= 0 ) ? main.substr( 0, slash ) : main;

								return path;
							},
							_view: function ( $location )
							{
								var main  = $location.path().substring( 1 );
								var split = main.split("/");

								return split.join("-");
							},
						}
					}
				);
		}
	)
	.directive(
		"blogCommentBox",
		function ()
		{
			return {
				restrict: "E",
				templateUrl: "views/blocks/blog-comment-box.html"
			}
		}
	)
	.directive(
		"blogShareButtons",
		function ()
		{
			return {
				restrict: "E",
				templateUrl: "views/blocks/blog-share-buttons.html"
			}
		}
	)
	.controller(
		"MenuController",
		[
			"$rootScope",
			"$scope",
			"$route",
			"$location",
			"$filter",
			"menuItems",
			function
			(
				$rootScope,
				$scope,
				$route,
				$location,
				$filter,
				menuItems
			)
			{
				$rootScope.location = $location;

				$scope.items = menuItems;

				$rootScope.$on(
					"$routeChangeSuccess",
					function ()
					{
						$scope.current = $filter( "filter" )( menuItems, { url: $route.current.locals._current } )[0];
						$scope._view = $route.current.locals._view;
						$scope._meta = $scope.getPageMeta();
					}
				);


				$scope.getPageMeta = function()
				{
					if ( !$scope.current )
						return {
							title: "",
							description: "",
						};

					var pageMeta = {};

					// global
					if ( $location.path() == "/" + $scope.current.url ) {
						pageMeta.title = ( $scope.current.meta.title ) ? $scope.current.meta.title + " - " : "";
						pageMeta.description = $scope.current.meta.description;

						return pageMeta;
					}

					// local
					var url  = $location.path().split("/");
					var last = url[url.length - 1];
					var parentTitle = ( $scope.current.meta.title ) ? $scope.current.meta.title + " - " : "";
					var activeChild = $filter( "filter" )( $scope.current.children, { url: last } )[0];

					pageMeta.title = activeChild.meta.title + " - " + parentTitle;
					pageMeta.description = activeChild.meta.description;

					return pageMeta;

				}


				$scope.focusOnNewsletter = function ()
				{
					if ( angular.element( "#input-news-desk" ).is( ":visible" ) )
						angular.element( "#input-news-desk" ).focus();
					else
						angular.element( "#input-news-mobile" ).focus();
				};

			}
		]
	);

})( angular );
