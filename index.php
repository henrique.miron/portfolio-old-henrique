<?php

$current = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$url     = parse_url($current);
$port    = (80 == $_SERVER['SERVER_PORT']) ? '' : ":{$_SERVER['SERVER_PORT']}";
$base    = str_replace('\\', '/', "{$url['scheme']}://{$url['host']}{$port}" . substr(__DIR__, strlen($_SERVER[ 'DOCUMENT_ROOT' ])) . '/');

$json  = file_get_contents('assets/json/menu.json');

if ( strpos( strtolower( $_SERVER['HTTP_USER_AGENT'] ), 'bot' ) !== false ) {
	$menus = json_decode($json);

	$page = str_replace($base, '', $current);

	foreach ($menus as $item) {
		if ($item->url == $page)
			$p = $item;
	}

	if (!isset($p))
		return;

	$index   = file_get_contents("index.php");
	$content = file_get_contents("views/{$p->url}.html");

	$index = trim(preg_replace('/(.*)<!-- remove -->/s', '', $index));
	$index = str_replace('<?php echo $base; ?>', $base, $index);
	$index = str_replace('{{ _meta.title }}', $p->meta->title, $index);
	$index = str_replace('{{ _meta.description }}', $p->meta->description, $index);

	die($index);
}

?>
<!-- remove -->
<!DOCTYPE html>
<html lang="pt-br" data-ng-app="4p" data-ng-controller="MenuController">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="google-site-verification" content="llGq1jOpKmXolLIsBl1m6EBkI0Tv-EQDuAyn8KGAwec" />

    <!-- Base URL -->
    <base href="<?php echo $base; ?>">

	<script>window.__menuItems = <?php echo $json; ?></script>

	<title>{{ _meta.title }}4Pixels Agência Digital</title>
	<meta name="description" content="{{ _meta.description }}">

	<link rel="stylesheet" href="assets/css/base.css">
	<link rel="stylesheet" href="https://file.myfontastic.com/3q7Pq3RTRDwdwSssys2Ac/icons.css">

	<script>window.__menuItems = <?php echo file_get_contents('assets/json/menu.json'); ?></script>

	<!-- Favicon -->

	<link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="assets/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="assets/img/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');

	fbq('init', '455250468011360');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=455250468011360&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-63570037-1', 'auto');
	</script>

	<!-- Start of Async HubSpot Analytics Code -->
	<script type="text/javascript">
	(function(d,s,i,r) {
	if (d.getElementById(i)){return;}
	var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
	n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/2291759.js';
	e.parentNode.insertBefore(n, e);
	})(document,"script","hs-analytics",300000);
	</script>
	<!-- End of Async HubSpot Analytics Code -->

	<!-- Smartlook -->
	<script type="text/javascript">
	    window.smartlook||(function(d) {
	    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
	    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
	    c.charset='utf-8';c.src='//rec.getsmartlook.com/recorder.js';h.appendChild(c);
	    })(document);
	    smartlook('init', '2392ed1ddd3e2d32f31dd1b815ea449c40c072a9');
	</script>
	<!-- Smartlook -->

</head>
<body data-ng-class="{'no-local-nav': !current.children || current.hideLocal}" class="view-{{_view}}">
	<div class="menu-mobile">
		<nav class="navbar navbar-light bg-faded">
			<a href="." class="logo navbar-brand">
				<img src="assets/img/4pixels-completo-sombra.png" alt="">
			</a>
			<button class="navbar-toggler pull-right" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">
				&#9776;
			</button>
			<div class="collapse navbar-toggleable-lg" id="exCollapsingNavbar2">
				<ul class="nav navbar-nav nav-default">

					<li data-ng-repeat="item in items" data-ng-class="{ active: item.url == current.url }" data-toggle="collapse" data-target="#exCollapsingNavbar2">
						<a href="{{ item.url }}" title="{{ item.text }}">{{ item.text }}</a>
					</li>

					<li>
						<form action="//ad4pixels.us1.list-manage.com/subscribe/post?u=695864990227c4ae7f9a55891&amp;id=934b42649f" method="post" class="newsletter-form">
							<input type="hidden" name="b_695864990227c4ae7f9a55891_934b42649f" tabindex="-1" value="">
							<label for="input-news-mobile">Assine nossa newsletter</label>
							<div class="input-group block-tablet">
								<input type="email" name="EMAIL" required placeholder="Seu e-mail" class="form-control" title="Por favor, verique se o e-mail está correto" id="input-news-mobile">
								<div class="input-group-btn">
									<button type="submit" class="btn btn-secondary">Enviar</button>
								</div>
							</div>
						</form>
					</li>

					<li>
						<address>
							<p>
								<a href="https://facebook.com/ad4pixels/" class="icon-4p-bdr" title="4Pixels no Facebook" target="_blank">
									<i class="fa fa-fw fa-facebook"></i>
								</a>
								<a href="https://www.linkedin.com/company/4pixels-ag%C3%AAncia-digital" class="icon-4p-bdr" title="4Pixels no LinkedIn" target="_blank">
									<i class="fa fa-fw fa-linkedin"></i>
								</a>
							</p>
							<p><a href="tel:011999469645">(11) 96184-8388</a></p>
							<p><a href="mailto:4p@ad4pixels.com.br">4p@ad4pixels.com.br</a></p>
						</address>
					</li>
				</ul>
			</div>
		</nav>
	</div>

	<div id="nav-global" class="nav-global menu-global">

		<svg id="nav-global-bdr" class="nav-global-bdr"></svg>

		<div class="nav-global-content">
			<ul class="nav nav-default">
				<li class="hidden-xs-down">
					<a href="." class="logo">
						<img src="assets/img/4pixels-completo-sombra.png" alt="">
					</a>
				</li>
				<li data-ng-repeat="item in items" data-ng-class="{ active: item.url == current.url }">
					<a href="{{ item.url }}" title="{{ item.text }}">{{ item.text }}</a>
				</li>
			</ul>
			<form action="//ad4pixels.us1.list-manage.com/subscribe/post?u=695864990227c4ae7f9a55891&amp;id=934b42649f" method="post" class="hidden-xs-down newsletter-form">
				<input type="hidden" name="b_695864990227c4ae7f9a55891_934b42649f" tabindex="-1" value="">
				<label for="input-news-desk">Assine nossa newsletter</label>
				<div class="input-group block-tablet">
					<input data-toggle="tooltip" data-placement="top" rel="txtTooltip" type="email" name="EMAIL" required placeholder="Seu e-mail" class="form-control" title="Por favor, verique se o e-mail está correto" id="input-news-desk">
					<div class="input-group-btn">
						<button type="submit" class="btn btn-secondary">Enviar</button>
					</div>
				</div>
			</form>
			<address class="hidden-xs-down">
				<p>
					<a href="https://facebook.com/ad4pixels/" class="icon-4p-bdr" title="4Pixels no Facebook" target="_blank">
						<i class="fa fa-fw fa-facebook"></i>
					</a>
					<a href="https://www.linkedin.com/company/4pixels-ag%C3%AAncia-digital" class="icon-4p-bdr" title="4Pixels no LinkedIn" target="_blank">
						<i class="fa fa-fw fa-linkedin"></i>
					</a>
				</p>
				<p><a href="tel:1122272203">(11) 96184-8388</a></p>
				<p><a href="mailto:4p@ad4pixels.com.br">4p@ad4pixels.com.br</a></p>
			</address>
		</div>
	</div>

	<div id="nav-local" class="nav-local">
		<ul class="nav">
			<li data-ng-repeat="item in current.children" data-ng-class="{ active: '/' + current.url + '/' + item.url == location.path() }">
				<a href="{{ current.url }}/{{ item.url }}" title="{{ item.text }}">
					<div class="card card-inverse">
						<div class="card-img-overlay" style="background-image: url({{ item.extras.bg }})">
							<h4 class="card-title">{{ item.text }}</h4>
						</div>
					</div>
				</a>
			</li>
		</ul>
	</div>

	<div id="content" class="content" data-ng-view></div>

	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script src="assets/js/any.js"></script>
	<script src="assets/js/angulartics.min.js"></script>
	<script src="assets/js/angulartics-ga.min.js"></script>
	<script src="assets/js/4p.js"></script>
	<script src="assets/js/bdr.js"></script>
	<script type="text/javascript" async src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-576b39e3379c44c7"></script>
	<script async>
		$(document).ready(function(){
		$('input[rel="txtTooltip"]').tooltip();
		});
	</script>
</body>
</html>
