##Primeiros passos

Após clonar o repositório, abra o seu terminal. Garanta que você tenha instalado o NodeJS e o NPM na sua máquina. Vá até a pasta `source`, localizada na raiz do projeto. Uma vez nessa pasta você verá dois arquivos: `package.json` e `Gruntfile.js`. O primeiro possui as dependências do projeto, e o segundo tem as configurações do Grunt.

O primeiro passo é instalar todas as dependências. Para isso, basta executar o seguinte código:

```
npm install # sudo talvez seja necessário em ambientes Unix
```

Agora você já tem todos os arquivos necessários para começar a desenvolver.

Durante o desenvolvimento, as tarefas do Grunt devem estar rodando, garantindo assim que o SASS seja automaticamente compilado e os arquivos JavaScript sejam copiados. Para iniciar o Grunt, basta digitar o comando:

```
grunt
```

Uma lista de tarefas irá iniciar e por último uma tarefa chamada `watch`, responsável por executar as tarefas necessárias sempre que um arquivo for atualizado. Mantenha o terminal aberto.

Adicionando dependências
Caso precise de novas dependências, é possível adicionar e atualizar o projeto. Como exemplo vamos adicionar o jQuery:

```
npm install jquery --save
```

Para que esse arquivo seja copiado para a pasta correta, devemos adicioná-lo à tarefa copy no arquivo Gruntfile.js.

```
jquery: {
    src: "node_modules/caminho/para/jquery.js",
    dest: "../assets/js/jquery.js",
},
```

Sempre que o arquivo Gruntfile.js for modificado é necessário encerrar o processo anterior e iniciar um novo. Para encerrar vá até seu terminal e aperte `Ctrl + C`.

Sempre atualize no git os arquivos package.json e Gruntfile.js, assim podemos sincronizar os arquivos para todos.


##Dependências

Para rodar o projeto é necessário ter instalado os componentes:
* node
* npm 
* ruby
* sass

Por favor, siga a exata ordem descrita abaixo.

## Node JS ##
1. Baixe a versão mais completa do node em https://nodejs.org/en/
2. Após baixar e instalar abra a linha de comando e escreva: "node -v"
3. OBS: na instalação do node já instalamos o NPM junto, fique atento nas opções do instalador.
4. Deve aparecer o número da versão do node, caso contrário houve algum erro na instalação.
5. Para verificar o npm digite "npm -v" e então deve aparecer o número da versão, caso contrário houve algum erro na instalação.

## NPM ##

*O NPM por padrão deve ser instalado com o node, o problema é que ele vem com uma versão antiga.*
 
1. Para atualizar abra a linha de comando e digite: "npm install -g npm"
2. Após rodar a instalação digite "npm -v" e veja se o número da versão é maior que o antigo, atualmente estamos na versão 3.9

## RUBY ##

1. O Ruby não depende nem do node e nem do NPM, para fazer a instalação basta acessar: http://rubyinstaller.org/ e baixar o instalador e seguir os passos

## SASS ##

***O SASS só pode ser instalado no ruby, para fazer isso é simples.***

1. Após instalar o ruby ele vai deixar na sua máquina um terminal exclusivo do ruby, não é o mesmo do windows.
2. Abra este terminal e execute o comando "gem install sass" com isso o sass já esta pronto para ser executado.